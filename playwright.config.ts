import { defineConfig } from "@playwright/test";

export default defineConfig({
    testDir: "./tests",
    fullyParallel: false,
    retries: 0,
    reporter: [["html", { open: "never" }]],
    use: {
        baseURL: "https://api.openbrewerydb.org"
    }
});
