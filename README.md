<h1>Technical Assessment - Richard Wattie</h1>
<h2>Setup</h2>
<h3>Install Node</h3>

Node can be installed from https://nodejs.org/en/download

<h3>Install Dependencies</h3>

```
npm install
```

N.B... For simply running these API tests you shouldn't be required to install the playwright browsers. But in case of any issues running the tests you may need to run...

```
playwright install
```

<h2>Executing Tests</h3>

To execute the tests, run

```
npm run tests
```

<h3>Show Report</h3>

To view the report, run
```
npm run report
```

<h1>Notes</h1>

The task didn't specify a duration in which to complete it in. I set myself a duration of 3 hours to include creating the project, tests and this documentation.

I split the scenarios into 2 test files. First one shows the initial tests and how I set it up before extending those tests to accommodate the second scenario.

The test `Check the brewery_type value returned is a valid type` returns a single failure due to a brewery_type being returned that is not documented.

<h1>Questions</h1>

<ul>
    <li>Describe the reason for the scope of your solution. Why did you test what you did, and why didn’t you test others?</li>
    <ul>
        <li>The scope of the solution was to aim for only positive test cases. I did this, so I could cover all the key points of the scenarios in my set time limit.</li>
    </ul>
    <li>If you had to support more features of the API, what would be the areas you would test first?</li>
    <ol>
        <li>Repeat the existing tests with various test data which included url encoding to ensure the api works in all aspects.</li>
        <li>From a very high level, check all other endpoints were at least successful and returned an expected body of data.</li>
        <li>Dive deeper into all the endpoints covering all the different parameters and various combinations</li>
        <li>Negative test cases, i.e. querying a single brewery with an invalid id</li>
    </ol>
    <li>After completing the technical challenge, what would you do differently if you were asked to do the same challenge again?</li>
    <ul>
        <li>Spend more time conducting manual exploratory testing to understand the API in more depth.</li>
        <li>Accommodate more tests i.e. negative cases</li>
    </ul>
</ul>
