export enum BreweryType {
    BrewPub = "brewpub",
    Closed = "closed",
    Contract = "contract",
    Micro = "micro",
    Nano = "nano",
    Planning = "planning",
    Proprietor = "proprietor",
    Regional = "regional"
}
