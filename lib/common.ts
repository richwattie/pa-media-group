import { Brewery } from "../types/Brewery";

export const isAscending = (arr: Brewery[], property: keyof Brewery) => arr.every((x, i) => i === 0 || x[property].toLowerCase() >= arr.at(i - 1)[property].toLowerCase());

export const formatParams = (params: any) => ({ ...params, sort: `${params.sort.property}:${params.sort.direction}` });
