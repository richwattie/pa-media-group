import { test, expect } from "@playwright/test";
import { Brewery } from "types/Brewery";
import { BreweryType } from "enums/BreweryType";
import { formatParams, isAscending } from "lib/common";

// Data objects.
const scenarios = [
    {
        title: "As a person from ‘Brighton (England)', I want to find ‘three’ local breweries organised by their ‘type’ in ‘ascending’ order",
        params: {
            by_city: "Brighton",
            by_country: "England",
            per_page: 3,
            sort: {
                property: "type",
                direction: "asc"
            }
        },
        requiresPagination: true
    },
    {
        title: "As a person from 'Brighton (England)', I want to be able to find different 'types' of breweries which are returned in 'ascending' order",
        params: {
            by_city: "Brighton",
            by_country: "England",
            sort: {
                property: "name",
                direction: "asc"
            }
        },
        requiresPagination: false
    }
];

// Loop through data objects and run all the tests for each object.
for (const scenario of scenarios) {
    // Groups together and describes a common set of tests.
    test.describe(scenario.title, () => {
        let body: Brewery[];

        /*
            Before each data object make the api request once to avoid having to repeat the same request for every test within the test.describe() block.
            beforeAll() is scoped to either the test.describe() block or the whole file. In this instance I am scoping it at the test.describe() level.
         */
        test.beforeAll(async ({ request, baseURL }) => {
            const response = await request.get(`${baseURL}/v1/breweries`, { params: formatParams(scenario.params) });

            expect(response.ok()).toBeTruthy();
            expect(response.status()).toBe(200);

            body = await response.json();
        });

        test(`Check the request returns exactly ${scenario.params.per_page} breweries`, async () => {
            // Skips the test based on a specific condition.
            test.skip(!scenario.params.per_page, "Ignore test if per_page property is not specified");

            // If expect() fails it will terminate the test immediately and report the failure in teh report.
            expect(body).toHaveLength(scenario.params.per_page);
        });

        test("Check the brewery_type value returned is a valid type", async () => {
            body.forEach((brewery) => {
                // If expect.soft() fails it will continue the test and report all failures in the report.
                expect
                    .soft(
                        Object.values(BreweryType).includes(brewery.brewery_type),
                        `"${brewery.brewery_type}" is not listed as a known type.\n\nBrewery: ${JSON.stringify(brewery)}\n\nAllowed types: ${Object.values(BreweryType)}`
                    )
                    .toBeTruthy();
            });
        });

        test(`Check the results are sorted by ${scenario.params.sort.property} in ascending order`, async () => {
            expect(isAscending(body, scenario.params.sort.property === "type" ? "brewery_type" : (scenario.params.sort.property as keyof Brewery))).toBeTruthy();
        });

        test("Check the results only return data from Brighton (England)", async () => {
            body.forEach((brewery) => {
                expect.soft(brewery.country).toEqual(scenario.params.by_country);
                expect.soft(brewery.city).toEqual(scenario.params.by_city);
            });
        });
    });

    // Groups together and describes a common set of tests.
    test.describe(`${scenario.title} - Meta`, () => {
        let metadata: {
            total: string;
            page: string;
            per_page: string;
        };

        /*
            Before each data object make the api request once to avoid having to repeat the same request for every test within the test.describe() block.
            beforeAll() is scoped to either the test.describe() block or the whole file. In this instance I am scoping it at the test.describe() level.
         */
        test.beforeAll(async ({ request, baseURL }) => {
            const response = await request.get(`${baseURL}/v1/breweries/meta`, { params: formatParams(scenario.params) });

            expect(response.ok()).toBeTruthy();
            expect(response.status()).toBe(200);

            metadata = await response.json();
        });

        test("Check the metadata per_page property value matches input parameter value", async () => {
            test.skip(!scenario.params.per_page, "Ignore test if per_page property is not specified");

            expect(metadata.per_page).toEqual(scenario.params.per_page.toString());
        });

        test("Check the metadata page property value returns 1", async () => {
            expect(metadata.page).toEqual("1");
        });

        test("Check results can be paginated", async ({ request, baseURL }) => {
            const totalPages = Math.ceil(parseInt(metadata.total) / parseInt(metadata.per_page));
            let currentPage = 0;
            let breweries: Brewery[] = [];

            // Make the api request and collate the data as many times as there are pages
            while (currentPage < totalPages) {
                currentPage++;
                const response = await request.get(`${baseURL}/v1/breweries`, {
                    params: {
                        ...formatParams(scenario.params),
                        page: currentPage
                    }
                });
                breweries.push(...(await response.json()));
            }

            expect(breweries).toHaveLength(parseInt(metadata.total));
            expect(currentPage).toEqual(scenario.requiresPagination ? totalPages : 1);
        });
    });
}
