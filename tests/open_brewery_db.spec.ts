import { test, expect } from "@playwright/test";
import { Brewery } from "types/Brewery";
import { isAscending } from "lib/common";

const params = {
    by_city: "Brighton",
    by_country: "England",
    per_page: 3,
    sort: "type:asc"
};

let body: Brewery[];

/*
    Before all tests in this file, make the api request once to avoid having to repeat the same request for every test.
    beforeAll() is scoped to either a test.describe() block or the whole file. In this instance I am scoping it at the file level.
 */
test.beforeAll(async ({ request, baseURL }) => {
    const response = await request.get(`${baseURL}/v1/breweries`, { params });

    expect(response.ok()).toBeTruthy();
    expect(response.status()).toBe(200);

    body = await response.json();
});

test("Check the request returns exactly 3 breweries", async () => {
    // If expect() fails it will terminate the test immediately and report the failure in teh report.
    expect(body).toHaveLength(params.per_page);
});

test("Check the results are sorted by type in ascending order", async () => {
    expect(isAscending(body, "brewery_type")).toBeTruthy();
});

test("Check the results only return data from Brighton (England)", async () => {
    body.forEach((brewery) => {
        // If expect.soft() fails it will continue the test and report all failures in the report.
        expect.soft(brewery.country).toEqual(params.by_country);
        expect.soft(brewery.city).toEqual(params.by_city);
    });
});
